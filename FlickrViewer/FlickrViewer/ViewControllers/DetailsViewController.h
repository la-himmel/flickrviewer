//
//  DetailsViewController.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrPhoto.h"

@interface DetailsViewController : UIViewController

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, assign) NSInteger currentIndex;

@end

