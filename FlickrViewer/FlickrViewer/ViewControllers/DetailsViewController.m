//
//  DetailsViewController.m
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import "DetailsViewController.h"
#import <iCarousel.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "CarouselSlide.h"
#import "FlickrPhotoSource.h"
#import "ObjectLoader.h"
#import "FlickrPhotoDetails.h"
#import "Constants.h"

@interface DetailsViewController()<iCarouselDelegate, iCarouselDataSource>

@property (nonatomic, weak) IBOutlet iCarousel *carousel;

@end

@implementation DetailsViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCarousel];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.carousel reloadData];
    [self.carousel scrollToItemAtIndex:self.currentIndex animated: false];
    self.carousel.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Nothing to do, because SDWebImage clears cache automatically
}

#pragma mark - Private methods

- (void)setupCarousel
{
    self.carousel.hidden = YES;
    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    self.carousel.type = iCarouselTypeLinear;
    self.carousel.pagingEnabled = YES;
    self.carousel.layer.masksToBounds = YES;
    self.carousel.bounces = NO;
}

- (void)loadPhotoWithId:(NSString *)photoId
{
    ObjectLoader *loader = [ObjectLoader sharedInstance];
    FlickrPhotoSource *flickr = [FlickrPhotoSource sharedInstance];

    typeof(self) __weak weakSelf = self;

    NSDictionary *parameters = [flickr photoParametersWithPhotoId:photoId];

    [loader itemsWithSource:flickr parameters:parameters completion:^(NSArray *items) {
        for (FlickrPhotoDetails *item in items) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"photoId == %@", item.photoId];
            FlickrPhoto *photo = [[weakSelf.photos filteredArrayUsingPredicate:predicate] firstObject];
            photo.photoDetails = item;
        }
        [weakSelf.carousel reloadItemAtIndex:weakSelf.carousel.currentItemIndex animated:YES];
    } failure:nil];
}

#pragma mark - iCarousel

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.photos count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view
{
    FlickrPhoto *photo = [self.photos objectAtIndex:index];
    FlickrPhotoDetails *details = photo.photoDetails;

    if (details == nil) {
        [self loadPhotoWithId:photo.photoId];
    }

    CarouselSlide *slide;
    if (view == nil) {
        slide = [[[NSBundle mainBundle] loadNibNamed:@"CarouselSlide" owner:self options:nil] firstObject];
        slide.frame = (CGRect){ CGPointZero, self.carousel.frame.size };
    } else {
        slide = (CarouselSlide *)view;
    }

    NSString *placeholderName = index % 2 ? placeholderImageOdd : placeholderImageEven;
    [slide.image sd_setImageWithURL:photo.imageUrl placeholderImage:[UIImage imageNamed:placeholderName]];

    slide.titleLabel.text = photo.title;
    slide.dateLabel.text = [[DetailsViewController defaultDateFormatter] stringFromDate:details.datePublished];
    slide.authorLabel.text = [details authorString];
    slide.viewsLabel.text = [details viewsString];

    return slide;
}

#pragma mark - Class methods

+ (NSDateFormatter *)defaultDateFormatter
{
    static NSDateFormatter *dateFormatter =  nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateFormatterShortStyle;
    });
    return dateFormatter;
}

@end
