//
//  CollectionViewController.m
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import "CollectionViewController.h"
#import "ObjectLoader.h"
#import "FlickrPhotoSource.h"
#import "PhotoCell.h"
#import "FlickrPhoto.h"
#import "DetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"

static NSString *reuseIdentifier = @"reuseIdentifier";
static NSString *segueIdentifier = @"showDetail";
static NSString *navigationTitleFormat = @"Photos with tag '%@'";
static NSString *enterTagMessage = @"Enter tag please";

@interface CollectionViewController ()

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSString *tag;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end

@implementation CollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tag = tagCatawiki;
    [self updateTitle];
    [self loadItems];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:segueIdentifier]) {
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
        DetailsViewController *vc = segue.destinationViewController;
        vc.currentIndex = indexPath.item;
        vc.photos = self.photos;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Nothing to do, because SDWebImage clears cache automatically
}

#pragma mark - Private methods

- (void)loadItems
{
    [self showIndicator];

    self.photos = nil;
    [self.collectionView reloadData];

    FlickrPhotoSource *flickr = [FlickrPhotoSource sharedInstance];
    NSDictionary *parameters = [flickr tagsParametersWithTag:self.tag];

    typeof(self) __weak weakSelf = self;

    [[ObjectLoader sharedInstance] itemsWithSource:flickr parameters:parameters completion:^(NSArray *items) {
        weakSelf.photos = items;
        [weakSelf.collectionView reloadData];
        [weakSelf hideIndicator];

    } failure:^(NSError *error) {
        [weakSelf hideIndicator];
        [weakSelf showErrorAlertWithMessage:noConnectionMessage];
    }];
}

- (void)updateTitle
{
    NSString *tagString = (self.tag == nil) ? tagCatawiki : self.tag;
    self.navigationItem.title = [NSString stringWithFormat:navigationTitleFormat, tagString];
}

- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];

    typeof(self) __weak weakSelf = self;
    [alert addAction:[UIAlertAction actionWithTitle:alertRetry style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf loadItems];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:alertCancel style:UIAlertActionStyleCancel handler:nil]];
    [alert.view setNeedsLayout];

    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)showTagPopup:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:enterTagMessage preferredStyle:UIAlertControllerStyleAlert];

    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = placeholderTag;
    }];

    typeof(self) __weak weakSelf = self;
    UIAlertAction *actionRetry = [UIAlertAction actionWithTitle:alertOK style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString *newTag = [[alert.textFields firstObject] text];
        if (![newTag isEqualToString:weakSelf.tag]) {
            weakSelf.tag = newTag;
            [weakSelf updateTitle];
            [weakSelf loadItems];
        }
    }];

    [alert addAction:actionRetry];
    [alert addAction:[UIAlertAction actionWithTitle:alertCancel style:UIAlertActionStyleCancel handler:nil]];
    [alert.view setNeedsLayout];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showIndicator
{
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
}

- (void)hideIndicator
{
    self.indicator.hidden = YES;
    [self.indicator stopAnimating];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.photos count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    PhotoCell *photoCell = (PhotoCell *)cell;
    FlickrPhoto *photo = [self.photos objectAtIndex:indexPath.item];

    NSString *placeholderName = indexPath.item % 2 ? placeholderImageOdd : placeholderImageEven;
    [photoCell.image sd_setImageWithURL:photo.imageUrl placeholderImage:[UIImage imageNamed:placeholderName]];

    return photoCell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:segueIdentifier sender:self];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemRawWidth = (self.view.bounds.size.width - 2) / 3;
    return CGSizeMake(itemRawWidth, itemRawWidth);
}

@end
