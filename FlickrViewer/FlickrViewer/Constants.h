//
//  Constants.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/19/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString *noConnectionMessage = @"The Internet connection appears to be offline.";
static NSString *tagCatawiki = @"catawiki";
static NSString *alertRetry = @"Retry";
static NSString *alertCancel = @"Cancel";
static NSString *placeholderTag = @"Tag";
static NSString *alertOK = @"OK";
static NSString *placeholderImageOdd = @"PlaceholderOdd";
static NSString *placeholderImageEven = @"PlaceholderEven";

#endif /* Constants_h */
