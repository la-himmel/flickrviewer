//
//  FlickrPhoto.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>
#import "FlickrPhotoDetails.h"

@interface FlickrPhoto : JSONModel

@property (nonatomic, strong) NSString *title;
@property (readonly, nonatomic, strong) NSURL *imageUrl;
@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, strong) FlickrPhotoDetails *photoDetails;

@end
