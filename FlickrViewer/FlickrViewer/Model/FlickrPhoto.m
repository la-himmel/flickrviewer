//
//  FlickrPhoto.m
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import "FlickrPhoto.h"

static NSString *imageUrlFormat = @"https://farm%@.static.flickr.com/%@/%@_%@.jpg";

@interface FlickrPhoto()

@property (nonatomic, strong) NSString *farm;
@property (nonatomic, strong) NSString *secret;
@property (nonatomic, strong) NSString *server;

@end

@implementation FlickrPhoto

#pragma mark - JSONModel

+ (JSONKeyMapper *)keyMapper
{
    NSDictionary *dictionary = @{ @"title": @"title", @"farm": @"farm", @"id": @"photoId",
                                  @"secret": @"secret", @"server": @"server" };
    return [[JSONKeyMapper alloc] initWithDictionary:dictionary];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

#pragma mark - Public methods

- (NSURL *)imageUrl
{
    NSString *urlString = [NSString stringWithFormat:imageUrlFormat, self.farm, self.server, self.photoId, self.secret];
    return [NSURL URLWithString:urlString];
}

@end
