//
//  FlickrPhotoDetails.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/19/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface FlickrPhotoDetails : JSONModel

@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, strong) NSDate *datePublished;
@property (nonatomic, strong) NSString *authorNickname;
@property (nonatomic, strong) NSString *authorName;
@property (nonatomic, assign) NSInteger views;

- (NSString *)authorString;

- (NSString *)viewsString;

@end
