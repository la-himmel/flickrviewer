//
//  FlickrPhotoDetails.m
//  FlickrViewer
//
//  Created by Ekaterina on 7/19/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import "FlickrPhotoDetails.h"

@interface FlickrPhotoDetails()

@property (nonatomic, assign) NSTimeInterval datePublishedInterval;

@end

@implementation FlickrPhotoDetails

#pragma mark - JSONModel

+ (JSONKeyMapper *)keyMapper
{
    NSDictionary *dictionary = @{ @"id": @"photoId", @"dates.posted": @"datePublishedInterval",
                                 @"owner.realname": @"authorName", @"owner.username": @"authorNickname",
                                 @"views": @"views" };
    return [[JSONKeyMapper alloc] initWithDictionary:dictionary];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

#pragma mark - Public methods

- (NSDate *)datePublished
{
    return [NSDate dateWithTimeIntervalSince1970:_datePublishedInterval];
}

- (NSString *)authorString
{
    if (self.authorName == nil || [self.authorName isEqualToString:@""]) {
        if (self.authorNickname != nil) {
            return [NSString stringWithFormat:@"@%@", self.authorNickname];
        }
        return nil;
    }

    if (self.authorNickname != nil && ![self.authorNickname isEqualToString:@""]) {
        NSString *nicknameString = [NSString stringWithFormat:@"(@%@)", self.authorNickname];
        return [NSString stringWithFormat:@"%@ %@", self.authorName, nicknameString];
    }

    return self.authorName;
}

- (NSString *)viewsString
{
    return [NSString stringWithFormat:@"Views: %ld", (long)self.views];
}

@end
