//
//  ObjectSource.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

@protocol ObjectSource <NSObject>

@required

// Returns base URL for source
- (NSURL *)baseUrl;

// Parses JSON dictionary into photos and returns photos array
- (NSArray *)itemsFromDictionary:(NSDictionary *)object;

@end