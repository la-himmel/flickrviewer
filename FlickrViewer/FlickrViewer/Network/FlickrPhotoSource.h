//
//  FlickrPhotoSource.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectSource.h"

@interface FlickrPhotoSource : NSObject<ObjectSource>

+ (instancetype)sharedInstance;

// ObjectSource protocol methods
- (NSURL *)baseUrl;

- (NSArray *)itemsFromDictionary:(NSDictionary *)object;

// Methods of this source
- (NSDictionary *)tagsParametersWithTag:(NSString *)tag;

- (NSDictionary *)photoParametersWithPhotoId:(NSString *)photoId;

@end
