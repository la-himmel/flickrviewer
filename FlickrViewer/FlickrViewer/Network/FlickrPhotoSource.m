//
//  FlickrPhotoSource.m
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import "FlickrPhotoSource.h"
#import "FlickrPhoto.h"
#import "Constants.h"

// Flickr API
static NSString *flickrBaseURLString = @"https://api.flickr.com/services/rest/";
static NSString *flickrAPIKey = @"cb44432e1d7ce137c2cb56c64aa006a2";

// Parameter keys
static NSString *apiKeyParameter = @"api_key";
static NSString *tagParameter = @"tags";
static NSString *methodParameter = @"method";
static NSString *formatParameter = @"format";
static NSString *noCallbackParameter = @"nojsoncallback";
static NSString *photoIdParameter = @"photo_id";

// Parameter values
static NSString *methodSearch = @"flickr.photos.search";
static NSString *methodGetInfo = @"flickr.photos.getInfo";
static NSString *format = @"json";
static NSString *noCallback = @"1";

// Parser keys
static NSString *keyPhotos = @"photos";
static NSString *keyPhoto = @"photo";

@implementation FlickrPhotoSource

#pragma mark - Class methods

+ (instancetype)sharedInstance
{
    static FlickrPhotoSource *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] init];
    });
    return sharedInstance;
}

+ (instancetype)alloc
{
    return [self sharedInstance];
}

#pragma mark - ObjectSource

- (NSURL *)baseUrl
{
    return [NSURL URLWithString:flickrBaseURLString];
}

- (NSArray *)itemsFromDictionary:(NSDictionary *)dictionary
{
    if (dictionary == nil || [[dictionary allKeys] count] == 0) {
        return nil;
    }

    if ([dictionary objectForKey:keyPhotos] != nil) {
        NSDictionary *object = [dictionary objectForKey:keyPhotos];
        NSArray *objects = [object objectForKey:keyPhoto];
        return [self photosFromArray:objects];
    }

    if ([dictionary objectForKey:keyPhoto] != nil) {
        NSDictionary *object = [dictionary objectForKey:keyPhoto];
        NSError *err = nil;
        FlickrPhotoDetails *details = [[FlickrPhotoDetails alloc] initWithDictionary:object error:&err];
        if (details) {
            return [NSMutableArray arrayWithObject:details];
        }
    }

    return nil;
}

#pragma mark - Parameters for different methods

- (NSDictionary *)tagsParametersWithTag:(NSString *)tag
{
    NSMutableDictionary *tagParameters = [NSMutableDictionary new];
    [tagParameters addEntriesFromDictionary:@{ methodParameter: methodSearch, tagParameter: (tag == nil ? tagCatawiki : tag) }];
    [tagParameters addEntriesFromDictionary:[self commonParameters]];
    return tagParameters;
}

- (NSDictionary *)photoParametersWithPhotoId:(NSString *)photoId
{
    NSMutableDictionary *tagParameters = [NSMutableDictionary new];
    [tagParameters addEntriesFromDictionary:@{ methodParameter: methodGetInfo, photoIdParameter: photoId}];
    [tagParameters addEntriesFromDictionary:[self commonParameters]];
    return tagParameters;
}

#pragma mark - Private methods

- (NSArray *)photosFromArray:(NSArray *)objects
{
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *dictionary in objects) {
        NSError *err = nil;
        FlickrPhoto *photo = [[FlickrPhoto alloc] initWithDictionary:dictionary error:&err];

        if (photo) {
            [result addObject:photo];
        }
    }
    return result;
}

- (NSDictionary *)commonParameters
{
    return @{ apiKeyParameter: flickrAPIKey, formatParameter: format, noCallbackParameter: noCallback };
}

@end
