//
//  ObjectLoader.m
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import "ObjectLoader.h"
#import <AFNetworking.h>

@implementation ObjectLoader

#pragma mark - Class methods

+ (instancetype)sharedInstance
{
    static ObjectLoader *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] init];
    });
    return sharedInstance;
}

+ (instancetype)alloc
{
    return [self sharedInstance];
}

#pragma mark - Methods for retrieving photos

- (void)itemsWithSource:(id<ObjectSource>)source parameters:(NSDictionary *)parameters
             completion:(void (^)(NSArray *))completion failure:(void (^)(NSError *))failure
{
    return [self itemsWithUrl:[source baseUrl] parameters:parameters
                       source:source
                   completion:completion
                      failure:failure];
}

#pragma mark - Private

- (void)itemsWithUrl:(NSURL *)url parameters:(NSDictionary *)parameters source:(id<ObjectSource>)source
          completion:(void (^)(NSArray *))completion failure:(void (^)(NSError *))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url.absoluteString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *dictionary = (NSDictionary *)responseObject;

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *result = [source itemsFromDictionary:dictionary];

            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion(result);
                }
            });
        });

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end
