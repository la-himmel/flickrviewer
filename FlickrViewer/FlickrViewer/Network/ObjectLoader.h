//
//  ObjectLoader.h
//  FlickrViewer
//
//  Created by Ekaterina on 7/17/16.
//  Copyright © 2016 Ekaterina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectSource.h"

@interface ObjectLoader : NSObject

// Returns the shared instance
+ (instancetype)sharedInstance;

// Downloads items and returns mapped objects
- (void)itemsWithSource:(id<ObjectSource>)source parameters:(NSDictionary *)parameters
             completion:(void (^)(NSArray *))completion failure:(void (^)(NSError *))error;

@end
