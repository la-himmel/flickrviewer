# README #

This project is created as assessment to get a job at Catawiki. 
This is a Flickr viewer that allows you to see all photos tagged with particular tags ('catawiki' is chosen by default). It also allows to see the photo fullscreen with some information about it. 

### About ###

* This project uses AFNetworking, SDWebImage, iCarousel and JSONModel libraries. 
* The dependencies are maintained using Cocoapods. 
* To start, run 'pod install && open FlickrViewer.xcworkspace'.

### Extension guidelines ###

* To add a new image source, create and implement a class that conforms the ObjectSource protocol.
* To add a new method for the source, create a dictionary with necessary parameters and make sure your source maintains the parsing for the data returned by this new method.

Author: Yekaterina Patrenina (Zhmud)